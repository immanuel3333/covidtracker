<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\Basecontroller;
use App\History;

class HistoryController extends Basecontroller
{

    public function history(){

        $histories = History::orderBy('id_user', 'asc')->get();
        
        return response()->json($histories);
    }

    public function history_user($id_user){

        $histories = History::where('id_user', $id_user)->orderBy('id_history', 'desc')->get();
        
        return response()->json($histories);
    }

    public function add_history(Request $request){

        $history = History::create([
            'nama_lokasi'  => $request->nama_lokasi,
            'id_user'      => $request->id_user
        ]);
        
        if ($history) {

            $response = [
                'History' => $history,
            ];

            return $this->responseOk($response);
        }else {
            return $this->responseError('Data Failed', 400);
        }
    }

    public function delete($id_history) {

        $history = History::where('id_history', $id_history)->delete();

        if($history){
            return $this->responseOk($history);
        }else{
            return $this->responseError('Failed', 400);
        }
    }
   
}
