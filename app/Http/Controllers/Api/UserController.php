<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\Basecontroller;
use App\Http\Requests\changeRequest;
use Illuminate\Support\Facades\DB;
use App\User;
use Validator;
use Auth;
use Hash;

class UserController extends Basecontroller
{
   public function login(Request $request)
   {
       $validator = Validator::make($request->all(), [
           'email' => ['required', 'string', 'email'],
           'password' => ['required', 'string'],
       ]);

       if ($validator->fails()){
           return $this->responseError('Login failed', 422, $validator->errors());
       }

       if(User::where(['email' => $request->email, 'password' => $request->password])->first()) {
           $user = User::where(['email' => $request->email, 'password' => $request->password])->first();
            
            $response = [
                'id' => $user->id,
                'email' =>$user->email,
                'token' => $user->createToken('MyToken')->accessToken,
                'name' => $user->name,
                'gambar' => url('/users//'.$user->gambar),
            
            ];

            return $this->responseOk($response);
       }else {
           return $this->responseError('Wrong email or password', 401);
       }
   }

   public function register(Request $request)
   {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'unique:users'],
            'password' => ['required', 'string', 'min:8' ],
            'password_confirm' => ['required','same:password'],
        ]);

        if ($validator->fails()){
            return $this->responseError('Registration failed', 422, $validator->errors());
        }

        $params = [
            'name' => $request->name,
            'email' => $request->email,
            'password' => ($request->password),
        ];
        
        if ($user = User::create($params)) {
            $token = $user->createToken('MyToken')->accessToken;

            $response = [
                'token' => $token,
                'user' => $user,
            ];

            return $this->responseOk($response);
        }else {
            return $this->responseError('Registration Failed', 400);
        }
   }

   public function profile(Request $request) 
   {
       return $this->responseOK($request->user());
   }

   public function change(changeRequest $request)
   {
       $email = $request->input('email');
       $password = $request->input('password');
       $name = $request->input('name');

       if(!$user = User::where('email', $email)->first()){
        return response([
            'message' => 'User doesn\'t exist!'
        ], 404);
    }
    if(!(User::where(['email' => $request->email, 'password' => $request->password])->first())) {
        return response([
            'message' => 'Invalid match!'
        ], 404);
    }

    if($request->gambar != null){
        $fileName = $request->gambar->getClientOriginalName();
        $path = $request->file('gambar')->move(public_path("/users"), $fileName);
        $photoURL = url('/users//'.$fileName);   

        User::where('email', $email)
            ->update([
                'gambar' => $fileName
            ]);
    }

    $user->password = $request->input('new_password');
    $user->name = $request->input('nama');
    $user->save();

    return response([
        'message' => 'success'
    ]);
   }

   
}
