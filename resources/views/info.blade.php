@include('layouts.navbar')


<div class="container">
  <div class="card-group mt-5 row justify-content-center">
    <div class="col-sm-6 col-md-4">
      <div class="card border-dark" style="height:385px">
        <img class="img-fluid" src="{{asset('images/Group 75.png')}}" style="border-radius:5px;padding:20px 20px 0px 20px">

        <div class="card-body ">
          <h5 class="card-title">Panduan Covid-19</h5>
          <p class="card-text" style="font-size:15px;padding-bottom:20px">Kenali gejala covid-19, cara pencegahan dan protokol kesehatan yang ditetapkan</p>

          <form class="text-center" method="get" action="{{ route('panduan') }}" >
              <button style="background-color:#71B5B6; border: white 3px solid; font-family: sans-serif;  font-size: 15px; color:white;border-radius:5px;padding:5px 20px 5px 20px" type="submit" id="btninfo1">Baca Selengkapnya</button>
          </form>
        </div>
      </div>
    </div>

    <div class="col-sm-6 col-md-4">
      <div class="card border-dark" style="height:385px">
        <img class="img-fluid" src="{{asset('images/Group 73.png')}}" style="border-radius:5px;padding:20px 20px 0px 20px">

        <div class="card-body">
          <h5 class="card-title">Rumah Sakit Rujukan</h5>
          <p class="card-text" style="font-size:15px;padding-bottom:20px">Informasi lokasi rumah sakit rujukan covid-19 di seluruh indonesia</p>
          
          <form class="text-center" method="get" action="{{ route('rujukan') }}">
            <button style="background-color:#71B5B6; border: white 3px solid; font-family: sans-serif;  font-size: 15px; color:white;border-radius:5px;padding:5px 20px 5px 20px" type="submit" id="btninfo2">Baca Selengkapnya</button>
          </form>
        </div>
      </div>
    </div>

    <div class="col-sm-6 col-md-4">
      <div class="card border-dark" style="height:385px">
        <img class="img-fluid" src="{{asset('images/Group 134.png')}}" style="border-radius:5px;padding:20px 20px 0px 20px">

        <div class="card-body">
          <h5 class="card-title">Kontak Layanan</h5>
          <p class="card-text" style="font-size:15px;padding-bottom:20px">Kontak layanan kementerian/lembaga untuk Covid-19</p>

          <form class="text-center" method="get" action="{{ route('kontak') }}">
            <button style="background-color:#71B5B6; border: white 3px solid; font-family: sans-serif;  font-size: 15px; color:white;border-radius:5px;padding:5px 20px 5px 20px" type="submit" id="btninfo3">Baca Selengkapnya</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<br>
@include('layouts.footer')
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
            <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
