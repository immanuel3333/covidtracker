<footer>
        <div class="container">
            <div class="row myFooter">
                <div class="col-12 col-md-6">
                <img id="gambarfooter" class="pl-5" width="250px" src="{{asset('images/logo footer.png')}}" >                     
                </div>
                <div class="col-4 col-md-2">
                    <p class="title">Info</p>
                    <a id="panduanfooter" href="{{ route('panduan') }}"  class="desc">Panduan Covid 19</a><br>
                    <a id="rujukanfooter" href="{{ route('rujukan') }}"  class="desc">Rumah Sakit Rujukan</a><br>
                    <a id="panggilanfooter" href="{{ route('kontak') }}"  class="desc">Pusat Panggilan</a><br>
                </div>
                <div class="col-4 col-md-2">
                    <p class="title">Tentang Kami</p>
                    <a id="latarbelakangfooter" href="{{ route('tentang') }}"  class="desc">Latar Belakang</a><br>
                    <a id="tujuanfooter" href="{{ route('tentang') }}"  class="desc">Tujuan</a><br>
                    <a id="timpengembangfooter" href="{{ route('tentang') }}"   class="desc">Tim Pengembang</a><br>
                </div>
                <div class="col-4 col-md-2">
                    <p class="title">Unduh</p>
                    <a id="unduhfooter" href="#"  class="desc">
                    <a href="{{asset('aplikasi/app-release.apk')}}">
                         <img class="img-fluid" src="{{asset('images/unduh.png')}}" >
                    </a>
           

                    </a><br>
                </div>
            </div>
        </div>

        
    </footer>

<!-- Option 1: Bootstrap Bundle with Popper -->


<!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
