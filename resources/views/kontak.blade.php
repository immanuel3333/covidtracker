  <!-- Nav -->
  @include('layouts.navbar')

  
  <div class="container">
        <div class="main-title text-center">
            <p style="font-weight:600;font-size:60px">Kontak Layanan</p>
        </div>
        </div>

  <br><br>
  <div class="container">
  <table class="table table-bordered">
  <thead style="background-color:#F8F9FA">
    <tr>
      <th scope="col" style="font-weight:400">No.</th>
      <th scope="col-4" style="font-weight:400">LAYANAN</th>
      <th scope="col" style="font-weight:400">EMAIL</th>
      <th scope="col" style="font-weight:400">KONTAK</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row" style="font-weight:400">1</th>
      <td>Posko Pengaduan Daring bagi 
          Masyarakat Terdampak Bencana 
          Nasional COVID-19 Ombudsman Pusat</td>
      <td>covid19-pusat@ombudsman.go.id</td>
      <td>WA: 0811-919-3737</td>
    </tr>
    <tr>
      <th scope="row" style="font-weight:400">2</th>
      <td>Layanan Pengaduan Bansos Kementrian</td>
      <td>bansoscovid19@kemsos.go.id</td>
      <td>Kontak layanan: 157<br>
          WA: 0811-10-222-10</td>
    </tr>
    <tr>
      <th scope="row" style="font-weight:400">3</th>
      <td>Layanan Pengaduan program keluarga harapan kementrian sosial </td>
      <td>Pengaduan@pkh.kemensos.go.id</td>
      <td>call center : 1500299<br>
           (senin-jumat 8.00 s.d. 17.00 WIB )</td>
    </tr>
    <tr>
      <th scope="row" style="font-weight:400">4</th>
      <td>Layanan Masyarakat Kartu Prakerja</td>
      <td>-</td>
      <td>call center : 021-25541246<br>
           (setiap hari kerja 08.00 s.d. 19.00 WIB)</td>
    </tr>
    <tr>
      <th scope="row" style="font-weight:400">5</th>
      <td>Bantuan Langsung Tunai Desa</td>
      <td>-</td>
      <td>call center : 1500040<br>
      SMS center : 0877-8899-0040 atau 0812-8899-0040</td>
    </tr>
    <tr>
      <th scope="row" style="font-weight:400">6</th>
      <td>Pembebasan Biaya Listrik (PLN)</td>
      <td>pln123@pln.co.id</td>
      <td>call center : 08122-123-123</td>
    </tr>
    <tr>
      <th scope="row" style="font-weight:400">7</th>
      <td>Tambahan insentif perumahan Kementerian PUPR</td>
      <td>informasi@pu.go.id</td>
      <td>-</td>
    </tr>
    <tr>
      <th scope="row" style="font-weight:400">8</th>
      <td>Informasi lebih lanjut mengenai perpajakan</td>
      <td>informasi@pajak.go.id</td>
      <td>Kring Pajak : 1500200</td>
    </tr>
    <tr>
      <th scope="row" style="font-weight:400">9</th>
      <td>Informasi lebih lanjut mengenai bea masuk dan cukai</td>
      <td>info@customs.go.id</td>
      <td>Bravo Bea Cukai : 1500225</td>
    </tr>
    <tr>
      <th scope="row" style="font-weight:400">10</th>
      <td>Informasi lebih lanjut mengenai Restrukturisasi Kredit atau <br>
      Pembiayaan terkait dampak COVID-19 Layanan Kontak OJK </td>
      <td>konsumen@ojk.go.id</td>
      <td>call center : 157<br>
          WA : 081-157-157-157</td>
    </tr>
    <tr>
      <th scope="row" style="font-weight:400">11</th>
      <td>Kejahatan Narkotika BNN</td>
      <td>-</td>
      <td>call center : 184<br>
          SMS/WA : 0812-2167-5675</td>
    </tr>
    <tr>
      <th scope="row" style="font-weight:400">12</th>
      <td>Pelayanan SEJIWA Kemen PPPA</td>
      <td>pengaduan@kemenpppa.go.id</td>
      <td>call center : 119 ext. 8 <br> WA : 0821-2575-1234 / 0811-1922-911<br>
      <a href="http://bit.ly/kamitetapada">http://bit.ly/kamitetapada</a></td>
    </tr>
    <tr>
      <th scope="row" style="font-weight:400">13</th>
      <td>Layanan Kementerian Agama Sigap COVID-19</td>
      <td>-</td>
      <td>WA : 081-1159-9003</td>
    </tr>
    <tr>
      <th scope="row" style="font-weight:400">14</th>
      <td>Informasi Fungsi Relawan Desa Lawan COVID-19 Kementerian Desa, PDT, dan Transmigrasi</td>
      <td>-</td>
      <td>call center : 1500040<br>
          SMS center : 0877-8899-0040 / 0812-8899-0040</td>
    </tr>
    <tr>
      <th scope="row" style="font-weight:400">15</th>
      <td>Jalur Pengaduan bagi Pelaku Pariwisata dan Ekonomi Kreatif Terdampak COVID-19</td>
      <td>info@kemenparekraf.go.id</td>
      <td>call center : 0811-8956-767<br>
      <a href="https://pedulicovid19.kemenparekraf.go.id/">https://pedulicovid19.kemenparekraf.go.id/</a></td>
    </tr>
    <tr>
      <th scope="row" style="font-weight:400">16</th>
      <td>Layanan Pengiriman Surat ke Kementerian Perdagangan</td>
      <td>persuratan@kemendag.go.id</td>
      <td>-</td>
    </tr>
    </tbody>
</table>
</div>


<!-- Footer -->
@include('layouts.footer')

<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="bootstrap/js/bootstrap.bundle.min.js"></script>
    
</body>
</html>