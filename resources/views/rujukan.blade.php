    @include('layouts.navbar')
  
    <div class="container">
            <h1 class="mt-4 mb-4 text-center"> Rumah Sakit Rujukan Covid-19</h2>
            <div class="table-responsive">
                <table id="data_users_reguler" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Nama Rumah Sakit</th>
                            <th>Wilayah</th>
                            <th>Telepon</th>
                            <th>Alamat</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($rujukan as $datacorona)
                        <tr>
                            <td>{{$datacorona["attributes"]["nama"]}}</td>
                            <td>{{$datacorona["attributes"]["wilayah"]}}</td>
                            <td>{{$datacorona["attributes"]["telepon"]}}</td>
                            <td>{{$datacorona["attributes"]["alamat"]}}</td>
                        </tr>
                    @endforeach
                        
                </table>
            </div>
        <script>
            $(document).ready(function() {
            $('#data_users_reguler').DataTable();
        } );
        </script>
        
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
@include('layouts.footer')
</body>
</html>