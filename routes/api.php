<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function() {
    Route::get('/profile', 'Api\UserController@profile');
});

Route::post('login', 'Api\UserController@login');
Route::post('register', 'Api\UserController@register');
Route::post('forgot', 'Api\ForgotPasswordController@forgot');
// Route::post('reset', 'Api\ForgotPasswordController@reset');
Route::get('history', 'Api\HistoryController@history');
Route::get('history/{id_user}', 'Api\HistoryController@history_user');
Route::post('/add/history', 'Api\HistoryController@add_history');
Route::delete('/history/{id_history}', 'Api\HistoryController@delete');
Route::post('/add/history', 'Api\HistoryController@add_history');
Route::post('change', 'Api\UserController@change');


//Route::get('/email/resend', 'Api\VerificationController@resend')->name('verificartion.resend');

//Route::get('/email/verify/{id}/{hash}','Api\VerificationController@verify')->name('verification.verify');



