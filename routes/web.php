<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();
Route::get('/','MapController@map')->name('home');
Route::get('/home','MapController@map')->name('home');
Route::get('/rujukan', 'rujukancontroller@index')->name('rujukan');
Route::get('/info', 'HomeController@info')->name('info');
Route::get('/tentang', 'HomeController@tentang')->name('tentang');
Route::get('/kontak', 'HomeController@kontak')->name('kontak');
Route::get('/panduan', 'HomeController@panduan')->name('panduan');
Route::get('/unduh', 'HomeController@tentang')->name('unduh');
